# LoginModal

All the async processes are implemented using reactive programming (Observables), most of data structures are described in Enums and Interfaces (see **.model.ts) of modules. Project has been created from scratch. 

User to test: 
* username: Test
* password: Test

## Development server

Run `npm i` and `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Structure

### Routing
Inludes basic routing with login module lazy-loaded

### Login component/page
Root component of login module includes login button and login counter component. Counter data is stored in localStorage and updated on each page load if there is a record, if there is no - creates record with zero values.

* Login component - `src\app\login\login.component.ts`
* Counter - `src\app\login\login-counter\login-counter.component.ts`
### Login modal
Once login button is pressed, a modal component is called through modal service. Modal component is made in a way that any component can be displayed inside of it passing ComponentRef and any context data.
It is possible to click "Forgot password?"/"User registration", a dummy components inside the modal will be opened.

* Modal component - `src\app\ui-lib\modal\modal.component.ts`
* Modal service - `src\app\ui-lib\modal\modal.service.ts`

### Ui components
Reusable global components/icons are included in UIlib module which can be imported in any component/module, so there is no need to import specific UI element module each time it's required in some place.

The project inlcudes custom form-field component, so all the logic for input like icon, label, error text or border is encapsulated there.

Icons are located in svg definition file and retrieved using icon component that accepts only icon name and can be used anywhere.

* Form-field component - `src\app\ui-lib\form-field\form-field.component.ts`
* Icon component - `src\app\ui-lib\icon\icon.component.ts`
* Icons definition - `src\assets\symbol-defs.svg`
* Global styles - `src\styles.less` 
* Constants - `src\app\theme\variables.less`

### Login process 

Validation of fields is onSubmit, required validation occurs onDirty. Once login button is pressed, login modal component passes credentials to login service where credentials validation occurs, it happens asynchronously using Subject to imitate REST call. After credentials validation login service increases corresponding counter, updates that in localStorage, reloads again to memory and displays updated value. 
