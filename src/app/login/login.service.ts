import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  ILoginCounter,
  ICredentials,
  IAuthSubject,
  IAuthError,
  IUsersData,
} from './login.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  counterSubject = new Subject<ILoginCounter>();
  authSubject = new Subject<IAuthSubject>();
  counterData: ILoginCounter;
  usersData: IUsersData;

  constructor(private http: HttpClient) {
    if (!localStorage.getItem('counterData')) {
      this.counterData = { successCount: 0, failureCount: 0 };
      localStorage.setItem('counterData', JSON.stringify(this.counterData));
    } else {
      this.updateCounterDataFromLS();
    }

    this.getUsersJSON().subscribe((result) => {
      this.usersData = result;
    });
  }

  getCounterSubject(): Observable<ILoginCounter> {
    return this.counterSubject.asObservable();
  }

  updateCounterDataFromLS(): void {
    const localStorageData = JSON.parse(localStorage.getItem('counterData'));

    this.counterData = {
      successCount: parseInt(localStorageData.successCount),
      failureCount: parseInt(localStorageData.failureCount),
    };

    this.counterSubject.next(this.counterData);
  }

  updateCounterData(isSuccess: boolean): void {
    if (isSuccess) {
      this.counterData.successCount += 1;
    } else {
      this.counterData.failureCount += 1;
    }
    localStorage.setItem('counterData', JSON.stringify(this.counterData));

    this.updateCounterDataFromLS();
  }

  getAuthSubject(): Observable<IAuthSubject> {
    return this.authSubject.asObservable();
  }

  submitCredentials(credentials: ICredentials) {
    const authResult = this.validateCredentials(credentials);
    this.authSubject.next(authResult);
    this.updateCounterData(authResult.isSuccess);
  }

  validateCredentials(credentials: ICredentials): IAuthSubject {
    const users = this.usersData.users;
    let errors: IAuthError[] = [];
    const user = users.find((user) => {
      return user.username === credentials.username;
    });

    if (!user) {
      errors.push({ fieldName: 'username', error: 'Not Found' });
      return { isSuccess: false, errors: errors };
    }

    if (user.password === credentials.password) {
      return { isSuccess: true };
    } else {
      errors.push({ fieldName: 'password', error: `Doesn't match` });
      return { isSuccess: false, errors };
    }
  }

  getUsersJSON(): Observable<IUsersData | any> {
    return this.http.get('./assets/usersData.json');
  }
}
