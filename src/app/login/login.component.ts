import { LoginModalComponent } from './login-modal/login-modal.component'
import { ModalService } from './../ui-lib/modal/modal.service'
import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
})
export class LoginComponent implements OnInit {
  constructor(private modalService: ModalService) {}

  onLoginClick(): void {
    this.modalService.displayModal(LoginModalComponent, {})
  }

  ngOnInit(): void {}
}
