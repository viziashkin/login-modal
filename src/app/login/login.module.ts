import { UiLibModule } from './../ui-lib/ui-lib.module'
import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { LoginComponent } from './login.component'
import { LoginModalComponent } from './login-modal/login-modal.component'
import { LoginCounterComponent } from './login-counter/login-counter.component'
import { LoginRoutingModule } from './login-routing.module'
import { ReactiveFormsModule } from '@angular/forms'
import { UserRegistrationComponent } from './user-registration/user-registration.component'
import { RestorePasswordComponent } from './restore-password/restore-password.component'

@NgModule({
  declarations: [
    LoginComponent,
    LoginModalComponent,
    LoginCounterComponent,
    UserRegistrationComponent,
    RestorePasswordComponent,
  ],
  imports: [ReactiveFormsModule, CommonModule, LoginRoutingModule, UiLibModule],
})
export class LoginModule {}
