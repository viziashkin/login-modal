import { ComponentFixture, TestBed } from '@angular/core/testing'

import { LoginCounterComponent } from './login-counter.component'

describe('LoginCounterComponent', () => {
  let component: LoginCounterComponent
  let fixture: ComponentFixture<LoginCounterComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginCounterComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginCounterComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
