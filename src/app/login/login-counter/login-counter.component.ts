import { ILoginCounter } from './../login.model'
import { LoginService } from './../login.service'
import { Component, OnInit } from '@angular/core'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-login-counter',
  templateUrl: './login-counter.component.html',
  styleUrls: ['./login-counter.component.less'],
})
export class LoginCounterComponent implements OnInit {
  counterData: ILoginCounter = { successCount: 0, failureCount: 0 }
  subscription: Subscription

  constructor(private loginService: LoginService) {
    this.counterData = this.loginService.counterData
    this.subscription = this.loginService
      .getCounterSubject()
      .subscribe((data) => {
        this.counterData = data
      })
  }

  ngOnInit(): void {}
}
