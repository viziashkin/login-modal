export interface ILoginCounter {
  successCount: number
  failureCount: number
}

export interface ICredentials {
  username: string
  password: string
}

export interface IUsersData {
  users: ICredentials[]
}

export interface IAuthError {
  fieldName: string
  error: string
}

export interface IAuthSubject {
  isSuccess: boolean
  errors?: IAuthError[]
}

export enum ELoginModalState {
  DEFAULT,
  SUCCESS,
}
