import { RestorePasswordComponent } from './../restore-password/restore-password.component'
import { UserRegistrationComponent } from './../user-registration/user-registration.component'
import { ModalService } from './../../ui-lib/modal/modal.service'
import { ICredentials, ELoginModalState, IAuthSubject } from './../login.model'
import { LoginService } from './../login.service'
import {
  ChangeDetectorRef,
  Component,
  HostBinding,
  HostListener,
  OnInit,
} from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.less'],
})
export class LoginModalComponent implements OnInit {
  loginForm: FormGroup
  authSubscription: Subscription
  state = ELoginModalState.DEFAULT
  states = ELoginModalState

  get name() {
    return this.loginForm.get('username')
  }

  get password() {
    return this.loginForm.get('password')
  }

  @HostBinding('class.app-login-modal') get getDefaultClass(): boolean {
    return true
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.handleCloseBtn()
  }

  constructor(
    private loginService: LoginService,
    private modalService: ModalService
  ) {
    this.initForm()
    this.authSubscription = this.loginService
      .getAuthSubject()
      .subscribe((res) => {
        this.onLoginResponse(res)
      })
  }

  onLoginResponse(result: IAuthSubject): void {
    if (result.isSuccess) {
      this.state = ELoginModalState.SUCCESS
      this.modalService.toggleCloseBtn(false)
      this.modalService.closeModal(true, 3000)
    } else {
      result.errors.forEach((error) => {
        this.loginForm.get(error.fieldName).setErrors({ noMatch: true })
      })
    }
  }

  initForm(): void {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    })
  }

  onSubmit(): void {
    if (!this.loginForm.valid) {
      this.loginForm.get('username').markAsDirty()
      this.loginForm.get('password').markAsDirty()

      return
    }
    const credentials: ICredentials = {
      username: this.loginForm.get('username').value,
      password: this.loginForm.get('password').value,
    }
    this.loginService.submitCredentials(credentials)
  }

  handleCloseBtn(): void {
    this.modalService.setCloseBtnColor(window.innerWidth > 768)
  }

  openUsrReg(): void {
    this.modalService.displayModal(UserRegistrationComponent, {})
  }

  openRestorePwd(): void {
    this.modalService.displayModal(RestorePasswordComponent, {})
  }

  ngOnInit(): void {
    this.handleCloseBtn()
  }

  ngOnDestroy(): void {
    this.authSubscription && this.authSubscription.unsubscribe()
  }
}
