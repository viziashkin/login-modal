import { Directive } from '@angular/core'

@Directive({
  selector: '[appButton]',
  host: {
    '[class.app-button]': 'true',
  },
})
export class ButtonDirective {
  constructor() {}
}
