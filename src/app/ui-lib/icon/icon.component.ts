import {
  Component,
  HostBinding,
  Input,
  OnInit,
  ViewEncapsulation,
} from '@angular/core'

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class IconComponent implements OnInit {
  @Input() name: string

  @HostBinding('class.app-icon') get getDefaultClass(): boolean {
    return true
  }

  constructor() {}

  ngOnInit(): void {}
}
