import { IconModule } from './icon/icon.module'
import { ButtonModule } from './button/button.module'
import { FormFieldModule } from './form-field/form-field.module'

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ModalModule } from './modal/modal.module'

@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    FormFieldModule,
    ButtonModule,
    IconModule,
  ],
  exports: [ModalModule, FormFieldModule, ButtonModule, IconModule],
})
export class UiLibModule {}
