import { Directive, HostBinding } from '@angular/core'
import { NgControl } from '@angular/forms'

@Directive({
  selector: '[appInput]',
})
export class InputDirective {
  @HostBinding('class.app-input') get getDefaultClass(): boolean {
    return true
  }

  constructor(public ngControl: NgControl) {}
}
