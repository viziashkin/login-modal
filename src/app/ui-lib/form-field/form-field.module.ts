import { IconModule } from './../icon/icon.module'
import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormFieldComponent } from './form-field.component'
import { InputDirective } from './input.directive'

@NgModule({
  imports: [CommonModule, IconModule],
  declarations: [FormFieldComponent, InputDirective],
  exports: [FormFieldComponent, InputDirective],
})
export class FormFieldModule {}
