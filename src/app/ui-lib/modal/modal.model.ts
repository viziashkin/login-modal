export interface IDynamicModalComponentData {
  component: any
  data?: any
}
export interface IModalConfig {
  isCloseBtnVisible?: boolean
  isCloseBtnDark?: boolean
}
