import { Injectable } from '@angular/core'
import { Observable, of, Subject } from 'rxjs'
import { IDynamicModalComponentData, IModalConfig } from './modal.model'

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  dataSubject = new Subject<IDynamicModalComponentData>()
  configSubject = new Subject<IModalConfig>()
  closeSubject = new Subject<boolean>()
  timer: any

  constructor() {}

  setCloseBtnColor(isDark: boolean): void {
    this.configSubject.next({ isCloseBtnDark: isDark })
  }

  toggleCloseBtn(isVisible: boolean): void {
    this.configSubject.next({ isCloseBtnVisible: isVisible })
  }

  displayModal(component: any, data: any): void {
    clearTimeout(this.timer)
    this.dataSubject.next({ component: component, data: data })
  }

  closeModal(isDelayed = false, timeout = 0): void {
    if (isDelayed) {
      this.timer = setTimeout(() => {
        this.closeModal()
      }, timeout)
    } else {
      clearTimeout(this.timer)
      this.dataSubject.next({ component: null })
      this.closeSubject.next(true)
    }
  }

  getDataSubject(): Observable<IDynamicModalComponentData> {
    return this.dataSubject.asObservable()
  }

  getConfigSubject(): Observable<IModalConfig> {
    return this.configSubject.asObservable()
  }

  getOnCloseSubject(): Observable<boolean> {
    return this.closeSubject.asObservable()
  }
}
