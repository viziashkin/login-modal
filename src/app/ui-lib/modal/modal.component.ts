import { IDynamicModalComponentData } from './modal.model'
import { ModalService } from './modal.service'
import { ModalDirective } from './modal.directive'
import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  HostListener,
  OnInit,
  ViewChild,
} from '@angular/core'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.less'],
})
export class ModalComponent implements OnInit {
  isOpen = false
  isCloseBtnDark = true
  isCloseBtnVisible = true
  dataSubscription: Subscription
  configSubscription: Subscription
  componentRef: ComponentRef<any>

  @ViewChild(ModalDirective, { static: true }) modalHost: ModalDirective

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private modalService: ModalService
  ) {
    this.dataSubscription = this.modalService
      .getDataSubject()
      .subscribe((result) => {
        if (result?.component) {
          this.openSelf(result)
        } else {
          this.closeSelf()
        }
      })

    this.configSubscription = this.modalService
      .getConfigSubject()
      .subscribe((result) => {
        if (result?.isCloseBtnDark !== undefined)
          this.isCloseBtnDark = result.isCloseBtnDark
        if (result?.isCloseBtnVisible !== undefined)
          this.isCloseBtnVisible = result.isCloseBtnVisible
      })
  }

  loadComponent(component: any, data: any): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      component
    )

    const viewContainerRef = this.modalHost.viewContainerRef
    viewContainerRef.clear()

    this.componentRef = viewContainerRef.createComponent(componentFactory)
    if (data) this.componentRef.instance['data'] = data
  }

  onOverlayClick(): void {
    this.closeSelf()
  }

  onCloseBtnClick(): void {
    this.closeSelf()
  }

  openSelf(context: IDynamicModalComponentData): void {
    this.isCloseBtnDark = true
    this.isCloseBtnVisible = true
    this.loadComponent(context.component, context.data)
    this.isOpen = true
  }

  closeSelf(): void {
    this.isOpen = false
    this.componentRef.destroy()
    this.modalHost.viewContainerRef.clear()
  }

  ngOnInit(): void {}
}
